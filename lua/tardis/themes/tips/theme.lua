local style = {
    style_id = "brun",
    style_name = "brun",
    font = "Trebuchet24",
    padding = 7.5,
    offset = 30,
    fr_width = 4,
    colors = {
        normal = {
            text = Color(255, 255, 255, 255),
            background = Color(91, 9, 243, 150),
            frame = Color(85, 85, 85, 150),
        },
        highlighted = {
            text = Color(255, 255, 255, 255),
            background = Color(91, 9, 243, 150),
            frame = Color(0, 187, 87, 100),
        }
    }
}
TARDIS:AddTipStyle(style)