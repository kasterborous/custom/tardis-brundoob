local PART={}
PART.ID = "brun_alever1"
PART.Name = "Brundoob A_Lever1"
PART.Control = "flight"
PART.Model = "models/brun/extension/a_lever1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2.8

if SERVER then
	function PART:Use(ply)
	    self:EmitSound( "brun/extension/a_levers.wav" )
	    TARDIS:Control("flight", ply)
	end
	function PART:Think()
		if not ( self:GetOn() ) or not self.exterior:GetData("power-state") then
			if self:GetSkin()==0 then return end
			self:SetSkin (0)
		elseif self:GetSkin ()~=1 then
			self:SetSkin(1)
		end
	end
end

--[[if CLIENT then
	function PART:Initialize()
		self.lever = {}
		self.lever.pos = 0
		self.lever.endpos = 0
	end

	function PART:Think()
		if self.lever.pos == 0 and self.exterior:GetData("flight", false) then
			self.lever.endpos = 1
		elseif self.lever.pos == 1 and not self.exterior:GetData("flight", false) then
			self.lever.endpos = 0
		end
		self.lever.pos = math.Approach(self.lever.pos, self.lever.endpos, FrameTime() * self.AnimateSpeed)
		self:SetPoseParameter("switch", self.lever.pos)
	end
end]]

TARDIS:AddPart(PART,e)