local PART={}
PART.ID = "brun_dpull5"
PART.Name = "Brundoob D_Pull 5"
PART.Model = "models/brun/extension/d_pull5.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.5

if SERVER then
	function PART:Use(ply)
		self:EmitSound("brun/extension/d_pull.wav")
		TARDIS:Control("repair", ply)
	end
end

TARDIS:AddPart(PART,e)