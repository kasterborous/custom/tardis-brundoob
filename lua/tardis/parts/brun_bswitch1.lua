local PART={}
PART.ID = "brun_bswitch1"
PART.Name = "Brundoob B_Switch1"
PART.Model = "models/brun/extension/b_switch1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4

if SERVER then
	function PART:Use(activator)
		local screen = self.interior:GetPart("brun_bscreen" )
		if screen:GetOn() then
			self:EmitSound( "brun/extension/a_switch.wav" )
			self:SetUseType(SIMPLE_USE)
			self.interior:ToggleScreens()
		end
	end
end

if CLIENT then
	function PART:Initialize()
		self.lever = {}
		self.lever.pos = 0
		self.lever.endpos = 0
	end

	function PART:Think()
		if self.lever.pos == 0 and self.interior:GetScreensOn() then
			self.lever.endpos = 1
		elseif self.lever.pos == 1 and not self.interior:GetScreensOn() then
			self.lever.endpos = 0
		end
		self.lever.pos = math.Approach(self.lever.pos, self.lever.endpos, FrameTime() * self.AnimateSpeed)
		self:SetPoseParameter("switch", self.lever.pos)
	end
end

TARDIS:AddPart(PART,e)