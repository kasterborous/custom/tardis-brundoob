local PART={}
PART.ID = "brun_doorl2"
PART.Name = "Brundoob doorl2"
PART.Model = "models/brun/extension/doorl2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.BypassIsomorphic = true

if SERVER then
	function PART:Collide()
		self:SetCollisionGroup(COLLISION_GROUP_NONE)
	end

	function PART:DontCollide()
		self:SetCollisionGroup(COLLISION_GROUP_WORLD)
	end

	function PART:Use()
		if ( self:GetOn() ) then
			self:Collide( true )
		else
			self:DontCollide( true )
		end
	end
end

TARDIS:AddPart(PART,e)