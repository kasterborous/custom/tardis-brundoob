local PART={}
PART.ID = "brun_consoletop"
PART.Name = "Brundoob's Console Top"
PART.Model = "models/brun/extension/console_top.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.BypassIsomorphic = true

if SERVER then
	function PART:Think()
		if self.exterior:GetData("power-state") then
			if self:GetSkin()==0 then return end
			self:SetSkin (0)
		elseif self:GetSkin ()~=1 then
			self:SetSkin(1)
		end
	end
end

TARDIS:AddPart(PART)